package app

import ujson.Obj

import java.time.{LocalDate, LocalDateTime, LocalTime}
import java.time.format.DateTimeFormatter
import scala.collection.{immutable, mutable}
import scala.util.Try
import com.typesafe.scalalogging.Logger

import scala.collection.mutable.ListBuffer

case class Times(startTime: String, endTime: String)

object TimeClock extends cask.MainRoutes{

  private val state =  new mutable.HashMap[String, mutable.SortedMap[String, Times]]
  private val logger = Logger(getClass)

  @cask.getJson("/update")
  def getUpdate(user:String, date:String, startTime:String, endTime:String): Obj =
    update(user, date, Times(startTime, endTime))

  @cask.postJson("/update")
  def postUpdate(user: String, date: String, startTime: String, endTime: String): Obj =
    update(user, date, Times(startTime, endTime))

  private def update(user:String, date:String, times:Times): Obj = {
    val errors= ListBuffer[String]()

    val parsedDate = Try {LocalDate.parse(date, DateTimeFormatter.ISO_DATE)}

    lazy val parsedStart = Try {LocalTime.parse(times.startTime, DateTimeFormatter.ISO_TIME)}
    lazy val parsedEnd   = Try {LocalTime.parse(times.endTime, DateTimeFormatter.ISO_TIME)}

    if (parsedDate.isFailure) {
      errors += s"Unable to parse date ${date} ISO format expected yyyy-mm-dd"
    }

    if (parsedStart.isFailure) {
      errors += s"Unable to parse startTime ${times.startTime} ISO time format expected HH:MM:SS"
    }

    if (parsedEnd.isFailure) {
      errors += s"Unable to parse endTime ${times.endTime} ISO time format expected HH:MM:SS"
    }

    if (parsedDate.isSuccess && parsedStart.isSuccess && parsedEnd.isSuccess) {
      if (parsedDate.get.isAfter(LocalDate.now())) {
        errors +=  s"date ${date} is in the future."
      } else if (LocalDateTime.of(parsedDate.get, parsedStart.get).isAfter(LocalDateTime.now)) {
        errors +=  s"startTime ${times.startTime} is in the future."
      } else if (LocalDateTime.of(parsedDate.get, parsedEnd.get).isAfter(LocalDateTime.now)) {
        errors +=  s"endTime ${times.endTime} is in the future."
      }
      if (parsedStart.get.isAfter(parsedEnd.get)) {
        errors +=  s"startTime ${times.startTime} is after ${times.endTime}"
      }
    }
    val allErrors = errors.toList
    if (allErrors.isEmpty) {
      var msg = ""
      /* Since state is mutable we will synchronize any access. */
      synchronized {

        state.get(user) match {
          case Some(userData) =>
            userData.get(date) match {
              case Some(dateEntry) =>
                logger.warn(s"User $user has overwritten times for ${date}")
                userData(date) = times
                msg = s"Times for date $date overwritten from start: ${dateEntry.startTime}, end: ${dateEntry.endTime}"
              case None =>
                userData.addOne(date, times)
                msg = "Times added for date $date"
            }
          case None =>
            val value = mutable.SortedMap[String, Times](date -> times)
            state.put(user, value)
            msg = s"New user $user added. Times added"
        }
      }
      ujson.Obj("user" -> user, "success" -> true, "message" -> msg)
    } else {
      ujson.Obj("user" -> user, "success" -> false, "message" -> allErrors)
    }
  }

  @cask.getJson("/report")
  def getReport(user: String): Obj = {
    logger.info(s"POST report called for user $user")
    report(user)
  }

  @cask.postJson("/report")
  def postReport(user: String): Obj = report(user)

  private def report(user: String): Obj = if (!state.contains(user)) {
      ujson.Obj("user" -> user, "success" -> false, "message" -> s"User $user doesn't exist")
    } else {
      ujson.Obj(
        "user"    -> user,
        "success" -> true,
        "dates"   -> state(user).toList.map {
          entry => ujson.Obj("date" -> entry._1, "startTime" -> entry._2.startTime, "endTime" -> entry._2.endTime)
        }
    )
  }

  initialize()
}
