package app

import io.undertow.Undertow
import utest._

import scala.collection.mutable.ArrayBuffer


object TimeClockTests extends TestSuite {

  final val Port = 8081
  final val Host = "localhost"

  def withServer[T](example: cask.main.Main)(f: String => T): T = {
    val server = Undertow.builder
      .addHttpListener(Port, Host)
      .setHandler(example.defaultHandler)
      .build
    server.start()
    val res =
      try f(s"http://$Host:$Port")
      finally server.stop()
    res
  }

  val tests: Tests = Tests {

    test("Bad Requests") - withServer(TimeClock) { host =>
      requests.get(s"$host/lost", check = false).statusCode ==> 404
      requests.delete(s"$host/update", check = false).statusCode ==> 405
      requests.post(s"$host/report", check = false).statusCode ==> 400
    }

    val user = "me"
    test("TimeClock") - withServer(TimeClock) { host =>

      val req = requests.get(s"$host/report?user=$user")

      req.contentType.get ==> "application/json"

      val json = ujson.read(req.text())

      json("user").value ==> user
      json("success").value ==> false
    }

    val updateJson = ujson.Obj(
      "user"      -> user,
      "date"      -> "2023-05-07",
      "startTime" -> "01:00:00",
      "endTime"   -> "05:00:00"
    )

    test("Happy Path") - withServer(TimeClock) { host =>
      val expected = ujson.Obj(
        "user"    -> user,
        "success" -> true,
        "message" -> s"New user $user added. Times added"
      )

      ujson.read(requests.post(s"$host/update", data=updateJson)) ==> expected
    }

    test("Add another day") - withServer(TimeClock) { host =>
      updateJson.update("date", "2023-05-06")
      val json = ujson.read(requests.post(s"$host/update", data=updateJson))
      json("success").value ==> true
    }

    test("Report") - withServer(TimeClock) { host =>
      val json = ujson.read(requests.get(s"$host/report?user=$user").text)
      json("dates").value.asInstanceOf[ArrayBuffer[String]].length ==> 2
      json("dates")(0)("date").value ==> "2023-05-06"
      json("dates")(1)("date").value ==> "2023-05-07"
      json("user").value ==> user
    }

    test("Bad date times") - withServer(TimeClock) { host =>
      val json = ujson.read(requests.post(
        s"$host/update", data = ujson.Obj(
          "user"      -> user,
          "date"      -> "wrong",
          "startTime" -> "wrong",
          "endTime"   -> "wrong"
        )
      ))

      json("user").value ==> user
      json("success").value ==> false
      json("message").value.asInstanceOf[ArrayBuffer[String]].length ==> 3
    }

    test("End before start") - withServer(TimeClock) { host =>
      updateJson.update("startTime", "01:00:00+02:00")
      updateJson.update("endTime", "00:00:00+02:00")
      val json = ujson.read(requests.post(s"$host/update", data = updateJson))
      json("success").value ==> false
      json("message").value.asInstanceOf[ArrayBuffer[ujson.Str]].head.toString.contains("after") ==> true
    }

    test ("Overwrite") - withServer(TimeClock) { host =>
      val json1 = ujson.read(requests.post(
        s"$host/update", data = ujson.Obj(
          "user" -> "overwrite",
          "date" -> "2020-02-02",
          "startTime" -> "00:00:00",
          "endTime" -> "00:00:05"
        )
      ))
      json1("success").value ==> true
      val json2 = ujson.read(requests.post(
        s"$host/update", data = ujson.Obj(
          "user" -> "overwrite",
          "date" -> "2020-02-02",
          "startTime" -> "00:00:01",
          "endTime" -> "00:00:05"
        )
      ))
      json2("success").value ==> true
      val json3 = ujson.read(requests.post(
        s"$host/report", data = ujson.Obj("user" -> "overwrite")
      ))
      json3("success").value ==> true
      json3("dates").value.asInstanceOf[ArrayBuffer[String]].length ==> 1
      json3("dates")(0)("date").value ==> "2020-02-02"
      json3("dates")(0)("startTime").value ==> "00:00:01"
    }
    // future date

  }
}